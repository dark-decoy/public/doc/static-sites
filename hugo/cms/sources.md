# Hugo CMS Sources

* [The Markdown CMS \| Tina](https://tina.io/ "The Markdown CMS | Tina")
* [Strapi \| Self\-Hosted Pricing \& Plans](https://strapi.io/pricing-self-hosted "Strapi | Self-Hosted Pricing \& Plans")
* [Hugo Configuration \| Front Matter](https://frontmatter.codes/docs/ssg-and-frameworks/hugo-configuration "Hugo Configuration | Front Matter")
* [decaporg\/one\-click\-hugo\-cms\: Hugo template with Decap CMS](https://github.com/decaporg/one-click-hugo-cms "decaporg/one-click-hugo-cms: Hugo template with Decap CMS")
* [netlify\/gotrue\: An SWT based API for managing users and issuing SWT tokens\.](https://github.com/netlify/gotrue "netlify/gotrue: An SWT based API for managing users and issuing SWT tokens.")
* [netlify\/git\-gateway\: A Gateway to Git APIs](https://github.com/netlify/git-gateway "netlify/git-gateway: A Gateway to Git APIs")
