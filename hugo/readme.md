# Hugo Notes

## Theme Style Categories
* [Podcast or News](./themes/news-style-or-podcast-style.md)
* [Recipe](./themes/recipe-style.md)
* [Realtor](./themes/realtor-style.md)

[Back](../readme.md)