# Hugo Theme Options for News Style or Podcast Style Sites

**Example Site:** https://www.nbcnews.com/

## Newsroom

**Repository:** https://github.com/onweru/newsroom

**Demo:** https://rooms.netlify.app/

## Blist

**Repository:** https://github.com/apvarun/blist-hugo-theme

**Demo:** https://blist.vercel.app/en/

## Travelify

**Repository:** https://github.com/balaramadurai/hugo-travelify-theme

**Demo:** https://balaramadurai.github.io/

## Geeky

**Repository:** https://github.com/statichunt/geeky-hugo

**Demo:** https://demo.statichunt.com/geeky-hugo/

## Northeast

**Repository:** https://github.com/y1zhou/hugo-northeast

**Demo:** https://www.y1zhou.com/

## Northend LAB

**Repository:** Theme Bundle

**Demo:** https://demo.gethugothemes.com/northendlab/

## BlogRa

**Repository:** https://github.com/rafed/BlogRa

**Demo:** https://rafed.github.io/devra/

## Hugo Creator Theme

**Repository:** https://github.com/CloudWithChris/hugo-creator

**Demo:** https://www.cloudwithchris.com/

## Gojournal

**Repository:** Theme Bundle

**Demo:** https://demo.gethugothemes.com/gojournal/

## Liva

**Repository:** Theme Bundle

**Demo:** https://gethugothemes.com/products/liva

## Revolve

**Repository:** Theme Bundle

**Demo:** https://gethugothemes.com/products/revolve

## Parsa

**Repository:** Theme Bundle

**Demo:** https://gethugothemes.com/products/parsa

## LogBook

**Repository:** Theme Bundle

**Demo:** https://gethugothemes.com/products/logbook

## Resources

* https://www.besthugothemes.com/
* https://themes.gohugo.io/
* https://gethugothemes.com/shop

[Back](../readme.md)