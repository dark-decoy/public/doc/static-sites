# Hugo Theme Options for News Style or Podcast Style Sites

**Example Site:** http://grandforkshomes.com

## Rough Current Site Layout
* Home page
  * About Me
    * Jack Wadhawan, CRS, GRI, ABR, SFT, Realtor®
    * Jack Wadhawan is the name that sells properties. Jack is a highly motivated agent with excellent negotiation skills. Jack's motto is to develop lasting relationships with clients by providing the best service to them.
  * Credentials and Qualifications
    * Outstanding record of performance. Jack has earned numerous top achievement awards year after year.
    * Top Producer since the year 2000.
    * Received several Gold and Platinum awards from Prudential Real Estate.
    * Hardworking -- a record number of open houses conducted every year.
    * Over 20 years of experience.
    * Experienced in both residential and commercial properties.
    * Certified Short-Sale Realtor.
    * CRS, GRI, and ABR designations. CRS and SFT are held by only about 2% of Realtors® in the United States.
  * Affiliations
    * National and State Association of Realtors®
    * Grand Forks Board of Realtors®
    * Licensed in North Dakota and Minnesota
    * Fargo Moorhead Area Association of Realtors
  * Featured Listings
* 2 iframe pages
* 3 multiple short article pages
* contact page
* side links to quick searches

## Main Points to Highlight
* Relationships
* Skilled
* Experienced

## Big things implement on the site
* iframe emdeded from mls
* email form or email link

## Orbitor

**Repository:** https://gethugothemes.com/products/orbitor

**Demo:** https://demo.gethugothemes.com/orbitor/

## Hugo Fresh

**Repository:** https://github.com/StefMa/hugo-fresh

**Demo:** https://hugo-fresh.vercel.app/

## Universal

**Repository:** https://github.com/devcows/hugo-universal-theme

**Demo:** https://devcows.github.io/hugo-universal-theme

## Bexer

**Repository:** https://gethugothemes.com/products/bexer

**Demo:** https://demo.gethugothemes.com/bexer/

## Wallet

**Repository:** https://gethugothemes.com/products/wallet

**Demo:** https://demo.gethugothemes.com/wallet/

## Hugo Serif Theme

**Repository:** https://github.com/zerostaticthemes/hugo-serif-theme

**Demo:** https://hugo-serif.netlify.app/

## Tailbliss

**Repository:** https://github.com/nusserstudios/tailbliss

**Demo:** https://tailbliss.netlify.app/

## Resources

* https://www.besthugothemes.com/
* https://themes.gohugo.io/
* https://gethugothemes.com/shop

[Back](../readme.md)