# Hugo Theme Options for News Style or Podcast Style Sites

**Example Site:** 
* https://www.kimgehrman.com/
* https://www.krenkrealestategroup.com/

## Rough Current Site Layout
* Home page
  * About Me
    * Jack Wadhawan, CRS, GRI, ABR, SFT, Realtor® is the name that sells properties. Jack is a highly motivated agent with excellent negotiation skills. Jack's motto is to develop lasting relationships with clients by providing the best service to them.
  * search box for listings
  * Credentials and Qualifications
    * Relationships
      * Top Producer since the year 2000.
    * Skilled
      * Hardworking – a record number of open houses conducted every year.
      * Received several Gold and Platinum awards from Prudential Real Estate.
      * Certified Short-Sale Realtor.
      * CRS, GRI, and ABR designations. CRS and SFT are held by only about 2% of Realtors® in the United States.
    * Experienced
      * Top Producer since the year 2000.
      * Over 20 years of experience.
      * Experienced in both residential and commercial properties.
  * Affiliations
    * National and State Association of Realtors®
    * Grand Forks Board of Realtors®
    * Licensed in North Dakota and Minnesota
    * Fargo Moorhead Area Association of Realtors
  * Featured Listings

## Main Points to Highlight
* Relationships
* Skilled
* Experienced

## Big things implement on the site
* iframe search box emdeded from mls
* email form or email link

## Adritian Free Hugo Theme

**Repository:** https://github.com/zetxek/adritian-free-hugo-theme

**Demo:** N/A

## Hugo Profile

**Repository:** https://github.com/gurusabarish/hugo-profile

**Demo:** https://hugo-profile.netlify.app/

## Pico

**Repository:** https://github.com/negrel/hugo-theme-pico

**Demo:** N/A

## Paige

**Repository:** https://github.com/willfaught/paige

**Demo:** https://willfaught.com/paige

## Resources

* https://www.besthugothemes.com/
* https://themes.gohugo.io/
* https://gethugothemes.com/shop

[Back](../readme.md)