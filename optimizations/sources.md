# Static Sites Optimization Sources

* [Introducing Jampack \- A new optimizer for static websites \- ‹div›RIOTS](https://divriots.com/blog/introducing-jampack/ "Introducing Jampack - A new optimizer for static websites - ‹div›RIOTS")
* [Our experience with Astro \- ‹div›RIOTS](https://divriots.com/blog/our-experience-with-astro "Our experience with Astro - ‹div›RIOTS")
* [Overview  \|  Lighthouse  \|  Chrome for Developers](https://developer.chrome.com/docs/lighthouse/overview/ "Overview  |  Lighthouse  |  Chrome for Developers")
* [Getting started with measuring Web Vitals  \|  Articles  \|  web\.dev](https://web.dev/articles/vitals-measurement-getting-started "Getting started with measuring Web Vitals  |  Articles  |  web.dev")
* [PageSpeed Insights](https://pagespeed.web.dev/analysis/https-grandforkshomes-com/jklilor96g?form_factor=desktop "PageSpeed Insights")
* [Overview of CrUX  \|  Chrome UX Report  \|  Chrome for Developers](https://developer.chrome.com/docs/crux "Overview of CrUX  |  Chrome UX Report  |  Chrome for Developers")
